package com.example.lulichka.sprinklefinance.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lulichka.sprinklefinance.App;
import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.R;
import com.example.lulichka.sprinklefinance.adapters.DashboardAdapter;
import com.example.lulichka.sprinklefinance.messages.AccountDeletedMessage;
import com.example.lulichka.sprinklefinance.messages.SelectionChangedMessage;
import com.example.lulichka.sprinklefinance.model.Transaction;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.inject.Inject;

public class DashboardFragment extends Fragment implements MainActivityMVP.DashboardView {
    @Inject
    MainActivityMVP.DashboardPresenter presenter;
    private DashboardAdapter transactionsAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private TextView tvBalanceEnd;
    private TextView tvBalanceStart;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        TextView textViewdateBegin = (TextView) view.findViewById(R.id.date_begin);
        TextView textViewDateEnd = (TextView) view.findViewById(R.id.date_end);
        tvBalanceEnd = (TextView) view.findViewById(R.id.tv_balance_end);
        tvBalanceStart = (TextView) view.findViewById(R.id.tv_balance_begin);
        Date end = new Date();
        Calendar c = new GregorianCalendar();
        c.add(Calendar.DATE, -30);
        Date dateBegin = c.getTime();
        SimpleDateFormat ft = new SimpleDateFormat("dd. MM. yyyy");
        String dateToStr1 = ft.format(end);
        String dateToStr2 = ft.format(dateBegin);
        textViewDateEnd.setText(dateToStr1);
        textViewdateBegin.setText(dateToStr2);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.dashboard_recyclerview);
        transactionsAdapter = new DashboardAdapter(getContext(), new ArrayList<Transaction>(0));
        mLinearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setAdapter(transactionsAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setView(this);
        transactionsAdapter.setPresenter(presenter);
        setTransactionsList(presenter.getTransactionsForLastDays());
        int currentBalance = presenter.getBalance();
        int balanceStart = presenter.getBalance30DaysBefore();
        tvBalanceEnd.setText(String.valueOf(currentBalance));
        tvBalanceStart.setText(String.valueOf(balanceStart));
    }

    @Override
    public void setTransactionsList(List<Transaction> transactionList) {
        transactionsAdapter.replaceData(transactionList);
        if (transactionList != null && transactionList.size() != 0) {
            LinearLayout linearLayout = (LinearLayout) getView().findViewById(R.id.ll);
            linearLayout.setVisibility(View.VISIBLE);
            LinearLayout linearLayout2 = (LinearLayout) getView().findViewById(R.id.ll2);
            linearLayout2.setVisibility(View.VISIBLE);
            View view = getView().findViewById(R.id.line);
            View view2 = getView().findViewById(R.id.line2);
            view.setVisibility(View.VISIBLE);
            view2.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showEmptyList() {
        Toast.makeText(getActivity(), "There are no transactions for past 30 days", Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onAccountDeleted(AccountDeletedMessage message) {
        setTransactionsList(presenter.getTransactionsForLastDays());
    }

    @Subscribe
    public void onSelectionChanged(SelectionChangedMessage message) {
        setTransactionsList(presenter.getTransactionsForLastDays());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

}
