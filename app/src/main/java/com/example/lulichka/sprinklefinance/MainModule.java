package com.example.lulichka.sprinklefinance;

import com.example.lulichka.sprinklefinance.data.MainModel;
import com.example.lulichka.sprinklefinance.data.MainRepository;
import com.example.lulichka.sprinklefinance.data.MemoryRepository;
import com.example.lulichka.sprinklefinance.presenters.MainActivityAccPresenter;
import com.example.lulichka.sprinklefinance.presenters.MainActivityDashPresenter;
import com.example.lulichka.sprinklefinance.presenters.MainActivityTransPresenter;

import dagger.Module;
import dagger.Provides;


@Module
public class MainModule {

    @Provides
    public MainActivityMVP.AccPresenter provideMainActivityPresenter(MainActivityMVP.Model model){
        return new MainActivityAccPresenter(model);
    }
    @Provides
    public MainActivityMVP.TransPresenter provideMainActivityATransPresenter(MainActivityMVP.Model model){
        return new MainActivityTransPresenter(model);
    }

    @Provides
    public MainActivityMVP.DashboardPresenter provideMainActivityDashPresenter(MainActivityMVP.Model model){
        return new MainActivityDashPresenter(model);
    }

    @Provides
    public MainActivityMVP.Model provideMainActivityModel(MainRepository repository){
        return new MainModel(repository);
    }

    @Provides
    public MainRepository provideMainRepository(){
        return new MemoryRepository();
    }
}
