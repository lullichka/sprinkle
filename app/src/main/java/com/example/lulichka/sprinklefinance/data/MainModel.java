package com.example.lulichka.sprinklefinance.data;

import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.ArrayList;
import java.util.List;


public class MainModel implements MainActivityMVP.Model {

    private MainRepository repository;

    public MainModel(MainRepository repository) {
        this.repository = repository;
    }

    @Override
    public void createAccount(String name) {
        repository.saveAccount(name);
    }

    @Override
    public UserAccount getAccount(String id) {
        return repository.getAccount(id);
    }

    @Override
    public List<UserAccount> getAccounts() {
        return repository.getAccounts();
    }

    @Override
    public void updateAccountByName(String id, String newName) {
        repository.updateAccountByName(id, newName);
    }

    @Override
    public void deleteAccount(String id) {
        repository.deleteAccount(id);
    }

    @Override
    public void createTransaction(String accountId, String title, int amount) {
        repository.createTransaction(accountId, title, amount);
    }

    @Override
    public void setSelected(String accountId, boolean selected) {
        repository.setSelected(accountId, selected);
    }

    @Override
    public UserAccount getSelectedAccounts() {
        return repository.getSelectedAccounts();
    }

    @Override
    public List<Transaction> getTransactions(UserAccount account) {
        List<Transaction> transactionList = new ArrayList<>();
        if (account != null) {
            transactionList = repository.getTransactions(account.getId());
        }
        return transactionList;
    }

    @Override
    public List<Transaction> getTransactionsForLastDays() {
        return repository.getTransactionsForLastDays();
    }

    @Override
    public int getBalanceOnEnd() {
        return repository.getCurrentBalance();
    }

    @Override
    public int getBalanceOnStart() {
        return repository.getBalance30DaysBefore();
    }

    @Override
    public void deleteTransaction(String transactionId) {
        repository.deleteTransaction(transactionId);
    }

    @Override
    public void updateTransaction(String transactionId, String newTitle, int newAmount) {
        repository.updateTransaction(transactionId, newTitle, newAmount);
    }

}
