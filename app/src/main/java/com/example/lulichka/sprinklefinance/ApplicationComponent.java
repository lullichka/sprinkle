package com.example.lulichka.sprinklefinance;

import com.example.lulichka.sprinklefinance.ui.AccountsFragment;
import com.example.lulichka.sprinklefinance.ui.DashboardFragment;
import com.example.lulichka.sprinklefinance.ui.MainActivity;
import com.example.lulichka.sprinklefinance.ui.TransactionsFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, MainModule.class})
public interface ApplicationComponent {

    void inject(App application);

    void inject(AccountsFragment accountsFragment);

    void inject(MainActivity activity);

    void inject(TransactionsFragment transactionsFragment);

    void inject(DashboardFragment dashboardFragment);

}
