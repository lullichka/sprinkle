package com.example.lulichka.sprinklefinance.data;


import android.util.Log;

import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;

public class MemoryRepository implements MainRepository {
    public final static String TAG = MemoryRepository.class.getSimpleName();

    @Override
    public UserAccount getAccount(String id) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<UserAccount> query = realm.where(UserAccount.class);
        query.equalTo("id", id);
        return query.findFirst();
    }

    @Override
    public void saveAccount(String name) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        if (getAccounts().size() == 0) {
            UserAccount userAccount = realm.createObject(UserAccount.class, UUID.randomUUID().toString());
            userAccount.setSelected(true);
            userAccount.setName(name);
            realm.commitTransaction();
        } else {
            UserAccount userAccount = realm.createObject(UserAccount.class, UUID.randomUUID().toString());
            userAccount.setName(name);
            realm.commitTransaction();
        }
    }

    @Override
    public List<UserAccount> getAccounts() {
        Realm realm = Realm.getDefaultInstance();
        List<UserAccount> userAccountList;
        RealmQuery<UserAccount> query = realm.where(UserAccount.class);
        userAccountList = query.findAll();
        return userAccountList;
    }

    @Override
    public void updateAccountByName(String id, String newName) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<UserAccount> query = realm.where(UserAccount.class);
        query.equalTo("id", id);
        UserAccount userAccount = query.findFirst();
        realm.beginTransaction();
        userAccount.setName(newName);
        realm.commitTransaction();
        Log.d(TAG, userAccount.getName());
    }

    @Override
    public void setSelected(String id, boolean selected) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<UserAccount> query = realm.where(UserAccount.class);
        query.equalTo("id", id);
        UserAccount userAccount = query.findFirst();
        realm.beginTransaction();
        userAccount.setSelected(selected);
        realm.commitTransaction();
    }

    @Override
    public UserAccount getSelectedAccounts() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(UserAccount.class).equalTo("isSelected", true).findFirst();

    }

    @Override
    public void deleteAccount(String id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        UserAccount userAccount = realm.where(UserAccount.class).equalTo("id", id).findFirst();
        List<Transaction> transactionList = realm.where(Transaction.class).equalTo("accountId", id).findAll();
        for (Transaction transaction : transactionList) {
            transaction.deleteFromRealm();
        }
        userAccount.deleteFromRealm();
        realm.commitTransaction();
    }

    @Override
    public void createTransaction(String accountId, String title, int amount) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Date date = new Date();
        Transaction transaction = realm.createObject(Transaction.class, UUID.randomUUID().toString());
        transaction.setTitle(title);
        transaction.setAmount(amount);
        transaction.setDate(date);
        transaction.setAccountId(accountId);
        UserAccount userAccount = realm.where(UserAccount.class).equalTo("id", accountId).findFirst();
        userAccount.getTransactions().add(transaction);
        int balance = userAccount.getBalance();
        balance += amount;
        userAccount.setBalance(balance);
        transaction.setBalanceAfterTransaction(balance);
        realm.commitTransaction();
        Log.d(TAG, userAccount.getName() + " updated on " + transaction.getTitle() + " on amount " + transaction.getAmount());
    }

    @Override
    public List<Transaction> getTransactions(String accountId) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Transaction> query = realm.where(Transaction.class);
        query.equalTo("accountId", accountId);
        List<Transaction> transactionList = query.findAll();
        return transactionList;
    }

    @Override
    public List<Transaction> getTransactionsForLastDays() {
        Realm realm = Realm.getDefaultInstance();
        UserAccount account = realm.where(UserAccount.class).equalTo("isSelected", true).findFirst();
        if (account != null) {
            String accountId = account.getId();
            RealmQuery<Transaction> query = realm.where(Transaction.class).equalTo("accountId", accountId);
            Date end = new Date();
            Calendar c = new GregorianCalendar();
            c.add(Calendar.DATE, -30);
            Date dateBegin = c.getTime();
            query.between("date", dateBegin, end);
            return query.findAll();
        } else return null;
    }

    @Override
    public void deleteTransaction(String transactionId) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmQuery<Transaction> query = realm.where(Transaction.class);
        query.equalTo("id", transactionId);
        Transaction transactionToDelete = query.findFirst();
        String userAccountId = transactionToDelete.getAccountId();
        RealmQuery<UserAccount> query1 = realm.where(UserAccount.class);
        UserAccount userAccount = query1.equalTo("id", userAccountId).findFirst();
        RealmList<Transaction> transactionList = userAccount.getTransactions();
        for (Transaction transaction1 : transactionList) {
            if (transaction1.getDate().after(transactionToDelete.getDate())) {
                int balance1 = transaction1.getBalanceAfterTransaction();
                balance1 -= transactionToDelete.getAmount();
                int balance = userAccount.getBalance();
                balance -= transactionToDelete.getAmount();
                transaction1.setBalanceAfterTransaction(balance1);
                userAccount.setBalance(balance);
            }
        }
        int balance = userAccount.getBalance();
        balance -= transactionToDelete.getAmount();
        userAccount.setBalance(balance);
        transactionToDelete.deleteFromRealm();
        realm.commitTransaction();
    }

    @Override
    public void updateTransaction(String transactionId, String newTitle, int newAmount) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmQuery<Transaction> query = realm.where(Transaction.class);
        query.equalTo("id", transactionId);
        Transaction transactionToUpdate = query.findFirst();
        String userAccountId = transactionToUpdate.getAccountId();
        RealmQuery<UserAccount> query1 = realm.where(UserAccount.class);
        UserAccount userAccount = query1.equalTo("id", userAccountId).findFirst();
        RealmList<Transaction> transactionList = userAccount.getTransactions();
        for (Transaction transaction1 : transactionList) {
            if (transaction1.getDate().after(transactionToUpdate.getDate())) {
                int oldBalanceAfter = transaction1.getBalanceAfterTransaction();
                int newBalance = oldBalanceAfter - transactionToUpdate.getAmount() + newAmount;
                transaction1.setBalanceAfterTransaction(newBalance);
            }
        }
        transactionToUpdate.setTitle(newTitle);
        int oldBalanceAfter = transactionToUpdate.getBalanceAfterTransaction();
        int newBalance = oldBalanceAfter - transactionToUpdate.getAmount() + newAmount;
        transactionToUpdate.setBalanceAfterTransaction(newBalance);
        transactionToUpdate.setAmount(newAmount);
        userAccount.setBalance(newBalance);
        realm.commitTransaction();
    }

    @Override
    public int getCurrentBalance() {
        Realm realm = Realm.getDefaultInstance();
        UserAccount account = realm.where(UserAccount.class).equalTo("isSelected", true).findFirst();
        if (account == null) {
            return 0;
        } else return account.getBalance();
    }

    @Override
    public int getBalance30DaysBefore() {
        Realm realm = Realm.getDefaultInstance();
        UserAccount account = realm.where(UserAccount.class).equalTo("isSelected", true).findFirst();
        if (account == null) {
            return 0;
        } else {
            int balance = account.getBalance();
            List<Transaction> transactionList = getTransactionsForLastDays();
            for (Transaction transaction : transactionList) {
                balance -= transaction.getAmount();
            }
            return balance;
        }
    }
}
