package com.example.lulichka.sprinklefinance.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.R;
import com.example.lulichka.sprinklefinance.messages.SelectionChangedMessage;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class UserAccountsAdapter extends RecyclerView.Adapter<UserAccountsAdapter.UserAccountsRecyclerViewHolder> {

    private MainActivityMVP.AccPresenter mPresenter;
    private Context mContext;
    private List<UserAccount> mAccountsList;
    private int mLastCheckedPosition = 0;

    public UserAccountsAdapter(Context context, List<UserAccount> accounts) {
        setList(accounts);
        mAccountsList = accounts;
        mContext = context;
    }

    public void replaceData(List<UserAccount> accounts) {
        setList(accounts);
        notifyDataSetChanged();
    }

    private void setList(List<UserAccount> accounts) {
        mAccountsList = accounts;
    }

    public void setPresenter(MainActivityMVP.AccPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public UserAccountsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View issueView = inflater.inflate(R.layout.accounts_rv_item, parent, false);
        return new UserAccountsRecyclerViewHolder(issueView, mPresenter);
    }

    @Override
    public void onBindViewHolder(UserAccountsRecyclerViewHolder holder, int position) {
        UserAccount account = mAccountsList.get(position);
        holder.name.setText(account.getName());
        holder.radioButton.setChecked(position == mLastCheckedPosition);
    }

    @Override
    public int getItemCount() {
        return mAccountsList == null ? 0 : mAccountsList.size();

    }

    class UserAccountsRecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        RadioButton radioButton;

        private UserAccountsRecyclerViewHolder(View itemView, final MainActivityMVP.AccPresenter presenter) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            radioButton = (RadioButton) itemView.findViewById(R.id.radio);
            mPresenter = presenter;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    UserAccount account = getItem(position);
                    mPresenter.onClick(account.getId());
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    UserAccount account = getItem(position);
                    mPresenter.onLongClick(account.getId());
                    return true;
                }
            });
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLastCheckedPosition = getAdapterPosition();
                    notifyItemRangeChanged(0, mAccountsList.size());
                    for (UserAccount account:mAccountsList){
                        mPresenter.setSelected(account.getId(), false);
                    }
                    UserAccount account = getItem(getAdapterPosition());
                    mPresenter.setSelected(account.getId(), true);
                    EventBus.getDefault().post(new SelectionChangedMessage());
                }
            });
        }

        private UserAccount getItem(int position) {
            return mAccountsList.get(position);
        }
    }
}


