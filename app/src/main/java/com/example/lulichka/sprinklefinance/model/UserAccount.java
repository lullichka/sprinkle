package com.example.lulichka.sprinklefinance.model;


import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UserAccount extends RealmObject {
    @PrimaryKey
    private String id;
    private String name;
    private boolean isSelected;
    private int balance;
    private RealmList<Transaction> transactions;

    public UserAccount() {
    }

    public UserAccount(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public RealmList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(RealmList<Transaction> transactions) {
        this.transactions = transactions;
    }

    public boolean isSelected() {
        return isSelected;
    }
}
