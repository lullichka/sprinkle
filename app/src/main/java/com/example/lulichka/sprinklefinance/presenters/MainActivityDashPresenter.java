package com.example.lulichka.sprinklefinance.presenters;


import android.support.annotation.Nullable;

import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.List;

public class MainActivityDashPresenter implements MainActivityMVP.DashboardPresenter {

    @Nullable
    private MainActivityMVP.DashboardView view;
    private MainActivityMVP.Model model;

    public MainActivityDashPresenter(MainActivityMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(MainActivityMVP.DashboardView view) {
        this.view = view;
    }

    @Override
    public List<Transaction> getTransactionsForLastDays() {
        List<Transaction> transactionList = model.getTransactionsForLastDays();
        return transactionList;
    }

    @Override
    public UserAccount getAccountById(String id) {
        return model.getAccount(id);
    }

    @Override
    public int getBalance() {
        return model.getBalanceOnEnd();
    }

    @Override
    public int getBalance30DaysBefore() {
        return model.getBalanceOnStart();
    }
}
