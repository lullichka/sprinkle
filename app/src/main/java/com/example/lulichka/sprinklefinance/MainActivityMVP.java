package com.example.lulichka.sprinklefinance;

import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.List;

public interface MainActivityMVP {

    interface AccView {

        void setAccountsList(List<UserAccount> accountsList);

        void showEmptyList();

        void showAccountDetailDialog(String accountId, String name);

        void showAccountDeleteDialog(String accountId, String name);
    }

    interface TransView {

        void setTransactionsList(List<Transaction> transactionList);

        void showMessage(String message);

        void showTransactionUpdateDialog(String transactionId, String title, int amount);

        void showTransactionDeleteDialog(String transactionId, String title);

    }

    interface DashboardView {

        void setTransactionsList(List<Transaction> transactionList);

        void showEmptyList();
    }

    interface AccPresenter {

        void setView(MainActivityMVP.AccView view);

        void addAccountButtonClicked(String accountName);

        List<UserAccount> getAccountsList();

        void onClick(String accountId);

        void onLongClick(String accountId);

        void setSelected(String accountId, boolean selected);

        void updateAccountButtonClicked(String accountId, String newName);

        void deleteAccountButtonClicked(String accountId);

    }

    interface TransPresenter {

        void setView(MainActivityMVP.TransView view);

        void addTransactionButtonClicked(String accountId, String title, int amount);

        List<Transaction> getCurrentTransactions();

        void onClick(Transaction transaction);

        UserAccount getAccountById(String id);

        void onLongClick(String transactionId, String title);

        void deleteTransactionButtonClicked(String transactionId);

        void updateTransactionButtonClicked(String transactionId, String title, int amount);

        List<UserAccount> getAccounts();

        UserAccount getSelectedAccounts();
    }

    interface DashboardPresenter {

        void setView(MainActivityMVP.DashboardView view);

        List<Transaction> getTransactionsForLastDays();

        UserAccount getAccountById(String id);

        int getBalance();

        int getBalance30DaysBefore();

    }

    interface Model {

        void createAccount(String name);

        UserAccount getAccount(String id);

        List<UserAccount> getAccounts();

        void updateAccountByName(String id, String newName);

        void deleteAccount(String id);

        void createTransaction(String accountId, String title, int amount);

        void setSelected(String accountId, boolean selected);

        UserAccount getSelectedAccounts();

        List<Transaction> getTransactions(UserAccount account);

        List<Transaction> getTransactionsForLastDays();

        int getBalanceOnEnd();

        int getBalanceOnStart();

        void deleteTransaction(String transactionId);

        void updateTransaction(String transactionId, String newTitle, int newAmount);

    }
}
