package com.example.lulichka.sprinklefinance.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.R;
import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TransactionsAdapter  extends RecyclerView.Adapter<TransactionsAdapter.TransactionsRecyclerViewHolder>{

    private MainActivityMVP.TransPresenter mPresenter;
    private Context mContext;
    private List<Transaction> mTransactionList;

    public TransactionsAdapter(Context context, List<Transaction> transactions) {
        setList(transactions);
        mTransactionList = transactions;
        mContext = context;
    }

    public void replaceData(List<Transaction> transactions) {
        setList(transactions);
        notifyDataSetChanged();
    }

    private void setList(List<Transaction> transactions) {
        mTransactionList = transactions;
    }

    public void setPresenter(MainActivityMVP.TransPresenter presenter){
        mPresenter = presenter;
    }

    @Override
    public TransactionsRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View issueView = inflater.inflate(R.layout.transactions_rv_item, parent, false);
        return new TransactionsRecyclerViewHolder(issueView, mPresenter);
    }

    @Override
    public void onBindViewHolder(TransactionsRecyclerViewHolder holder, int position) {
        Transaction transaction = mTransactionList.get(position);
        UserAccount account = mPresenter.getAccountById(transaction.getAccountId());
        holder.title.setText(transaction.getTitle());
        Date date = transaction.getDate();
        SimpleDateFormat ft = new SimpleDateFormat("dd. MM. yyyy");
        String dateToStr = ft.format(date);
        holder.date.setText(dateToStr);
        holder.amount.setText(String.valueOf(transaction.getAmount()));
        holder.accountName.setText(account.getName());
        holder.balance.setText(String.valueOf(transaction.getBalanceAfterTransaction()));
    }

    @Override
    public int getItemCount() {
        return mTransactionList == null ? 0 : mTransactionList.size();
    }

    class TransactionsRecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView date;
        TextView amount;
        TextView accountName;
        TextView balance;

        private TransactionsRecyclerViewHolder(View itemView, final MainActivityMVP.TransPresenter presenter) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_title);
            date = (TextView) itemView.findViewById(R.id.tv_date);
            amount = (TextView) itemView.findViewById(R.id.tv_amount);
            accountName = (TextView)itemView.findViewById(R.id.tv_account_name);
            balance = (TextView)itemView.findViewById(R.id.tv_balance);
            mPresenter = presenter;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    Transaction transaction = getItem(position);
                    presenter.onClick(transaction);
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    int position = getAdapterPosition();
                    Transaction transaction = getItem(position);
                    presenter.onLongClick(transaction.getId(), transaction.getTitle());
                    return true;
                }
            });
        }

        private Transaction getItem(int position) {
            return mTransactionList.get(position);
        }
    }
}
