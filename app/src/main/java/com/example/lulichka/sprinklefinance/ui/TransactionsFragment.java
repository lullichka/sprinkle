package com.example.lulichka.sprinklefinance.ui;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lulichka.sprinklefinance.App;
import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.R;
import com.example.lulichka.sprinklefinance.adapters.TransactionsAdapter;
import com.example.lulichka.sprinklefinance.messages.SelectionChangedMessage;
import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class TransactionsFragment extends Fragment implements MainActivityMVP.TransView {
    public final static String TAG = TransactionsFragment.class.getSimpleName();

    FloatingActionButton floatingActionButton;
    @Inject
    MainActivityMVP.TransPresenter presenter;
    private TransactionsAdapter transactionsAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private Spinner spinner;
    private UserAccount selectedAccount;

    public TransactionsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transactions, container, false);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.transactions_recyclerview);
        transactionsAdapter = new TransactionsAdapter(getContext(), new ArrayList<Transaction>(0));
        mLinearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setAdapter(transactionsAdapter);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddTransactionDialog();
            }
        });
        return view;
    }

    private void showAddTransactionDialog() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_transaction, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText title = (EditText) view.findViewById(R.id.title);
        final EditText amount = (EditText) view.findViewById(R.id.amount);
        spinner = (Spinner) view.findViewById(R.id.choose_acc_spinner);
        setItemsToSpinner();
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String amount1 = amount.getText().toString().trim();
                if (amount1.isEmpty() || amount1.length() == 0 || amount1.equals("") || amount1 == null) {
                    showMessage("You can't create transaction without amount!");
                } else {
                    if (selectedAccount != null) {
                        String accountId = selectedAccount.getId();
                        presenter.addTransactionButtonClicked(accountId, title.getText().toString(),
                                Integer.parseInt(amount.getText().toString()));
                    } else {
                        showMessage("Create and choose account first");
                        dialog.dismiss();
                    }
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.height = ActionBar.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.7f;
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setAttributes(lp);
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

    private void setItemsToSpinner() {
        final List<UserAccount> accountsList = presenter.getAccounts();
        if (accountsList.size() != 0) {
            final List<String> accountsNames = new ArrayList<>();
            for (UserAccount account : accountsList) {
                accountsNames.add(account.getName());
            }
            ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                    android.R.layout.simple_spinner_dropdown_item, accountsNames);
            spinner.setAdapter(adapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedAccount = accountsList.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    selectedAccount = accountsList.get(0);
                }
            });
        } else {
            showMessage("Choose accounts");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.setView(this);
        transactionsAdapter.setPresenter(presenter);
        setTransactionsList(presenter.getCurrentTransactions());
        Log.d(TAG, String.valueOf(presenter.getCurrentTransactions().size()));
    }

    @Override
    public void setTransactionsList(List<Transaction> transactionList) {
        transactionsAdapter.replaceData(transactionList);
        if (transactionList.size() == 0) {
            showMessage("There are no transactions yet");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onSelectionChanged(SelectionChangedMessage message) {
        setTransactionsList(presenter.getCurrentTransactions());
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showTransactionUpdateDialog(final String transactionId, String title, int amount) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_transaction, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView message = (TextView) view.findViewById(R.id.text_choose_message);
        message.setText(view.getResources().getString(R.string.update_transaction));
        final EditText editTextTitle = (EditText) view.findViewById(R.id.title);
        final EditText editTextAmount = (EditText) view.findViewById(R.id.amount);
        Spinner spinner = (Spinner) view.findViewById(R.id.choose_acc_spinner);
        spinner.setVisibility(View.INVISIBLE);
        editTextTitle.setText(title);
        editTextAmount.setText(String.valueOf(amount));
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.updateTransactionButtonClicked(transactionId, editTextTitle.getText().toString(),
                        Integer.parseInt(editTextAmount.getText().toString()));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.height = ActionBar.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.7f;
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setAttributes(lp);
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

    @Override
    public void showTransactionDeleteDialog(final String transactionId, String title) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_delete_account, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        TextView message = (TextView) view.findViewById(R.id.text_dialog_message);
        message.setText(getActivity().getString(R.string.delete_transaction));
        final TextView account = (TextView) view.findViewById(R.id.account_name);
        account.setText(title);
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.deleteTransactionButtonClicked(transactionId);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.height = ActionBar.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.7f;
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setAttributes(lp);
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

}
