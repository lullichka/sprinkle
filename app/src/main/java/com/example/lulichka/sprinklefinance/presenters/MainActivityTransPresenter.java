package com.example.lulichka.sprinklefinance.presenters;


import android.support.annotation.Nullable;

import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.List;

public class MainActivityTransPresenter implements MainActivityMVP.TransPresenter {
    @Nullable
    private MainActivityMVP.TransView view;
    private MainActivityMVP.Model model;

    public MainActivityTransPresenter(MainActivityMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(MainActivityMVP.TransView view) {
        this.view = view;
    }

    @Override
    public void addTransactionButtonClicked(String  accountId, String title,  int amount) {
        model.createTransaction(accountId, title, amount);
        view.setTransactionsList(getCurrentTransactions());
    }

    @Override
    public List<Transaction> getCurrentTransactions() {
        UserAccount account= model.getSelectedAccounts();
        List<Transaction> transactionList =model.getTransactions(account);
        view.setTransactionsList(transactionList);
        return transactionList;
    }

    @Override
    public void onClick(Transaction transaction) {
       view.showTransactionUpdateDialog(transaction.getId(), transaction.getTitle(), transaction.getAmount());
    }

    @Override
    public UserAccount getSelectedAccounts() {
        return model.getSelectedAccounts();
    }

    @Override
    public UserAccount getAccountById(String id) {
        return model.getAccount(id);
    }

    @Override
    public void onLongClick(String transactionId, String title) {
        view.showTransactionDeleteDialog(transactionId, title);
    }

    @Override
    public void deleteTransactionButtonClicked(String transactionId) {
        model.deleteTransaction(transactionId);
        view.setTransactionsList(getCurrentTransactions());
    }

    @Override
    public void updateTransactionButtonClicked(String transactionId, String title, int amount) {
        model.updateTransaction(transactionId, title, amount);
        view.setTransactionsList(getCurrentTransactions());
    }

    @Override
    public List<UserAccount> getAccounts() {
        return model.getAccounts();
    }


}
