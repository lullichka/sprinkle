package com.example.lulichka.sprinklefinance.adapters;


import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.R;
import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.DashboardRecyclerViewHolder> {
    private MainActivityMVP.DashboardPresenter mPresenter;
    private Context mContext;
    private List<Transaction> mTransactionList;

    public DashboardAdapter(Context context, List<Transaction> transactions) {
        setList(transactions);
        mTransactionList = transactions;
        mContext = context;
    }

    public void replaceData(List<Transaction> transactions) {
        setList(transactions);
        notifyDataSetChanged();
    }

    private void setList(List<Transaction> transactions) {
        mTransactionList = transactions;
    }

    public void setPresenter(MainActivityMVP.DashboardPresenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public DashboardRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View issueView = inflater.inflate(R.layout.dashboard_item, parent, false);
        return new DashboardRecyclerViewHolder(issueView);
    }

    @Override
    public void onBindViewHolder(DashboardRecyclerViewHolder holder, int position) {
        Transaction transaction = mTransactionList.get(position);
        UserAccount account = mPresenter.getAccountById(transaction.getAccountId());
        holder.title.setText(transaction.getTitle());
        holder.amount.setText(String.valueOf(transaction.getAmount()));
        if (transaction.getAmount() < 0) {
            holder.amount.setTextColor(Color.RED);
        }
        if (account != null) {
            holder.accountName.setText(account.getName());
        }
    }

    @Override
    public int getItemCount() {
        return mTransactionList == null ? 0 : mTransactionList.size();
    }

    class DashboardRecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        TextView amount;
        TextView accountName;

        private DashboardRecyclerViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.tv_name);
            amount = (TextView) itemView.findViewById(R.id.tv_amount);
            accountName = (TextView) itemView.findViewById(R.id.tv_account_name);
        }

    }
}
