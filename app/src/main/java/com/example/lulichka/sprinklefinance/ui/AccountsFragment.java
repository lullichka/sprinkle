package com.example.lulichka.sprinklefinance.ui;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lulichka.sprinklefinance.App;
import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.R;
import com.example.lulichka.sprinklefinance.adapters.UserAccountsAdapter;
import com.example.lulichka.sprinklefinance.messages.AccountDeletedMessage;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;


public class AccountsFragment extends Fragment implements MainActivityMVP.AccView {
    public final static String TAG = AccountsFragment.class.getSimpleName();
    FloatingActionButton floatingActionButton;
    @Inject
    MainActivityMVP.AccPresenter presenter;
    private UserAccountsAdapter mAccountsAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    public AccountsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((App) getActivity().getApplication()).getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.accounts_recyclerview);
        mAccountsAdapter = new UserAccountsAdapter(getContext(), new ArrayList<UserAccount>(0));
        mLinearLayoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLinearLayoutManager);
        recyclerView.setAdapter(mAccountsAdapter);
        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fab);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddAccountDialog();
            }
        });
    }

    private void showAddAccountDialog() {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_account, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText account = (EditText) view.findViewById(R.id.account_name);
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.addAccountButtonClicked(account.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.height = ActionBar.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.7f;
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setAttributes(lp);
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_accounts, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAccountsAdapter.setPresenter(presenter);
        presenter.setView(this);
        setAccountsList(presenter.getAccountsList());
    }

    @Override
    public void setAccountsList(List<UserAccount> accountsList) {
        mAccountsAdapter.replaceData(accountsList);
        if (accountsList.size() == 0) {
            showEmptyList();
        }
    }

    @Override
    public void showEmptyList() {
        Toast.makeText(getActivity(), "No Accounts", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAccountDetailDialog(final String id, String name) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_add_account, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final EditText account = (EditText) view.findViewById(R.id.account_name);
        account.setText(name);
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.updateAccountButtonClicked(id, account.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.height = ActionBar.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.7f;
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setAttributes(lp);
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);

    }

    @Override
    public void showAccountDeleteDialog(final String accountId, final String name) {
        View view = getActivity().getLayoutInflater().inflate(R.layout.dialog_delete_account, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final TextView account = (TextView) view.findViewById(R.id.account_name);
        account.setText(name);
        builder.setView(view);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.deleteAccountButtonClicked(accountId);
                EventBus.getDefault().post(new AccountDeletedMessage());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.height = ActionBar.LayoutParams.WRAP_CONTENT;
        lp.dimAmount = 0.7f;
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setAttributes(lp);
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }


}
