package com.example.lulichka.sprinklefinance.presenters;

import android.support.annotation.Nullable;

import com.example.lulichka.sprinklefinance.MainActivityMVP;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.List;


public class MainActivityAccPresenter implements MainActivityMVP.AccPresenter {
    public final static String TAG = MainActivityAccPresenter.class.getSimpleName();

    @Nullable
    private MainActivityMVP.AccView view;
    private MainActivityMVP.Model model;

    public MainActivityAccPresenter(MainActivityMVP.Model model) {
        this.model = model;
    }

    @Override
    public void setView(MainActivityMVP.AccView view) {
        this.view = view;
    }

    @Override
    public void addAccountButtonClicked(String name) {
        model.createAccount(name);
        view.setAccountsList(model.getAccounts());
    }

    @Override
    public List<UserAccount> getAccountsList() {
        List<UserAccount> userAccounts = model.getAccounts();
        view.setAccountsList(userAccounts);
        return userAccounts;
    }

    @Override
    public void onClick(String accountId) {
        UserAccount account = model.getAccount(accountId);
        view.showAccountDetailDialog(accountId, account.getName());
    }

    @Override
    public void onLongClick(String accountId) {
        UserAccount account = model.getAccount(accountId);
        view.showAccountDeleteDialog(accountId, account.getName());
    }

    @Override
    public void setSelected(String accountId, boolean selected) {
        model.setSelected(accountId, selected);
    }

    @Override
    public void updateAccountButtonClicked(String accountId, String newName) {
        model.updateAccountByName(accountId, newName);
        view.setAccountsList(model.getAccounts());
    }

    @Override
    public void deleteAccountButtonClicked(String accountId) {
        model.deleteAccount(accountId);
        view.setAccountsList(model.getAccounts());
    }

}
