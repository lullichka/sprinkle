package com.example.lulichka.sprinklefinance.data;

import com.example.lulichka.sprinklefinance.model.Transaction;
import com.example.lulichka.sprinklefinance.model.UserAccount;

import java.util.List;

public interface MainRepository {

    UserAccount getAccount(String id);

    void saveAccount(String name);

    List<UserAccount> getAccounts();

    void updateAccountByName(String id, String newName);

    void setSelected(String id, boolean selected);

    UserAccount getSelectedAccounts();

    void deleteAccount(String id);

    void createTransaction(String accountId, String title, int amount);

    List<Transaction> getTransactions(String accountId);

    List<Transaction> getTransactionsForLastDays();

    void deleteTransaction( String transactionId);

    void updateTransaction(String accountId, String title, int amount);

    int getCurrentBalance();

    int getBalance30DaysBefore();
}
